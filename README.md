# Powerpuff-girlz

### Checklist:

#### [ ] Supervised Learning
        - [ ] Tabular
        - [ ] Text

#### [ ] Unsupervised Learning (Biasanya clustering)
        - [ ] Tabular
        - [ ] Text

#### [ ] Deep Learning: CNN
        - [ ] Image preprocessing : mungkin kita bisa lihat gray levelnya, naikin intensitas dll -> mungkin bisa coba ikutin kaya Lab1 pengcit
        - [ ] image Augmentation : gimana cara nyamain jumlah image di tiap kelas tiap kelas
        - [ ] Transfer Learning : accuracy metrics yang dipakai apa

#### [ ] Deep Learning: RNN for time series
